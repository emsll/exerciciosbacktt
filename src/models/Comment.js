const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
  username: {
    type: DataTypes.STRING,
    allowNull: false

  },

  description: {
    type: DataTypes.STRING,
    allowNull: false

  },

},{

});

Comment.associate = function(models) {
  Comment.belongsTo(models.User);

}

module.exports = Comment;