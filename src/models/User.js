const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
}, {

});

User.associate = function(models) {
    User.hasMany(models.Comment, { });
    User.belongsToMany(models.User, {through: 'Like', as: 'liking', foreignKey: 'likeId'});
  
}

module.exports = User;