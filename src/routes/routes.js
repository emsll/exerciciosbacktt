const { Router } = require('express');
const UserController = require('../controllers/UserController');
const router = Router();

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.put('/users/:id',UserController.update);
router.delete('/users/:id',UserController.destroy);

router.put('/like/:id',UserController.like);
// router.get('/usercoments/:id',UserController.userComments);

const CommentController = require('../controllers/CommentController');

router.get('/comments',CommentController.index);
router.get('/comments/:id',CommentController.show);
router.post('/comments',CommentController.create);
router.put('/comments/:id',CommentController.update);
router.delete('/comments/:id',CommentController.destroy);

module.exports = router;
