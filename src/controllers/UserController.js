const { response } = require('express');
const User = require('../models/User');
const Comment = require('../models/Comment');
const {Op} = require('sequelize');

const create = async(req,res) => {
    try{
        const user = await User.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
    }catch(err){
        res.status(500).json({error: err});
    }

};

const index = async(req,res) => {
    try {
        const users = await User.findAll ({
            include: [
                'liking',
                'liked'
            ]
        });
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({users});
    } 

};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk (id);
        return res.status(200).json({user});
    }catch (err){
        return res.status(500).json({err});
    }

};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [update] = await User.update(req.body, {where: {id: id}});
        if (update) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }

};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if (deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        } 
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }

};

const like = async(req,res) => {
    const {id} = req.params;
    try {
        const commentLiking = await User.findByPk(req.body.LikeId);
        const userLiked = await User.findByPk(id);
        await userLiked.addLiking(commentLiking);
        return res.status(200).json(commentLiking);
    }catch(err){
        return res.status(500).json({err});
    }

};


module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    like

};