require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedComment = require('./CommentSeeder');

(async () => {
  try {

    await seedUser();
    await seedComment();

  } catch(err) {console.log(err)}
  
})();
