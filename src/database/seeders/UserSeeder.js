const User = require("../../models/User");
const faker = require('faker-br');

 const seedUser = async function() {
   try {
     await User.sync({force: true});
     const users = [];

     for (let i = 0; i < 10; i++) {

      let user = await User.create({
        email: faker.internet.email(),
        name: faker.name.lastName(),
        createdAt: new Date(),
        updatedAt: new Date()
      });

      if(i>1){
        let userLiked = await User.findByPk(i-1);
        user.addLiking(userLiked);
      }

    }

  } catch (err) {console.log(err +'!');}
}

module.exports = seedUser;